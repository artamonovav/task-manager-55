package ru.t1.artamonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.DataBase64SaveRequest;

@Component
public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-base64";

    @NotNull
    private static final String DESCRIPTION = "Save data in base64 file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE BASE64]");
        @NotNull DataBase64SaveRequest request = new DataBase64SaveRequest(getToken());
        domainEndpointClient.saveDataBase64(request);
    }

}
