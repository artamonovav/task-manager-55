package ru.t1.artamonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.UserRemoveRequest;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.util.TerminalUtil;

@Component
public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-remove";

    @NotNull
    private static final String DESCRIPTION = "user remove";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken());
        request.setLogin(login);
        userEndpointClient.removeUser(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
