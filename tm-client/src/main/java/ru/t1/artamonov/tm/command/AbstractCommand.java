package ru.t1.artamonov.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.api.model.ICommand;
import ru.t1.artamonov.tm.api.service.IServiceLocator;
import ru.t1.artamonov.tm.enumerated.Role;

@Getter
@Setter
@Component
public abstract class AbstractCommand implements ICommand {

    @Nullable
    @Autowired
    protected IServiceLocator serviceLocator;

    @NotNull
    public abstract String getName();

    @Nullable
    protected String getToken() {
        return serviceLocator.getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        serviceLocator.getTokenService().setToken(token);
    }

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getDescription();

    public abstract void execute();

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ": " + description + "\n";
        return result;
    }

}
