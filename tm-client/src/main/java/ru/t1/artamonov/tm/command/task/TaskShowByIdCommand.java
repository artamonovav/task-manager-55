package ru.t1.artamonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.model.TaskDTO;
import ru.t1.artamonov.tm.dto.request.TaskGetByIdRequest;
import ru.t1.artamonov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-show-by-id";

    @NotNull
    private static final String DESCRIPTION = "Display task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setTaskId(id);
        @Nullable final TaskDTO task = taskEndpointClient.getTaskById(request).getTask();
        showTask(task);
    }

}
