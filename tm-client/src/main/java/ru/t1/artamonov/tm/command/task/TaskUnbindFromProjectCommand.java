package ru.t1.artamonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.t1.artamonov.tm.util.TerminalUtil;

@Component
public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-unbind-from-project";

    @NotNull
    private static final String DESCRIPTION = "Unbind task from project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @Nullable final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setTaskId(taskId);
        request.setProjectId(projectId);
        taskEndpointClient.unbindTaskFromProject(request);
    }

}
