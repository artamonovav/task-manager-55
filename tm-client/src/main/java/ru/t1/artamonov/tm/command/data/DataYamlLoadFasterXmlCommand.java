package ru.t1.artamonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.DataYamlLoadFasterXmlRequest;

@Component
public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml";

    @NotNull
    private static final String DESCRIPTION = "Load data from yaml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD YAML]");
        @NotNull DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(getToken());
        domainEndpointClient.loadDataYamlFasterXml(request);
    }

}
