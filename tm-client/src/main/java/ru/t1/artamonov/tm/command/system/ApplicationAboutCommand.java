package ru.t1.artamonov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.ServerAboutRequest;
import ru.t1.artamonov.tm.dto.response.ServerAboutResponse;

@Component
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String DESCRIPTION = "Display developer info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("[Client]");
        System.out.println("Name: " + propertyService.getAuthorName());
        System.out.println("E-mail: " + propertyService.getAuthorEmail());
        System.out.println("[Server]");
        @NotNull final ServerAboutRequest request = new ServerAboutRequest(getToken());
        @NotNull final ServerAboutResponse response = serviceLocator.getSystemEndpoint().getAbout(request);
        System.out.println("Name:  " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

}
