package ru.t1.artamonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.model.TaskDTO;
import ru.t1.artamonov.tm.dto.request.TaskListByProjectIdRequest;
import ru.t1.artamonov.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-show-by-project-id";

    @NotNull
    private static final String DESCRIPTION = "Show task list by project id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken());
        @Nullable final List<TaskDTO> tasks = taskEndpointClient.listTaskByProjectId(request).getTasks();
        renderTasks(tasks);
    }

}
