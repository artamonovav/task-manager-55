package ru.t1.artamonov.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.repository.model.ISessionRepository;
import ru.t1.artamonov.tm.api.service.model.ISessionService;
import ru.t1.artamonov.tm.exception.entity.ModelNotFoundException;
import ru.t1.artamonov.tm.exception.entity.UserNotFoundException;
import ru.t1.artamonov.tm.exception.field.IdEmptyException;
import ru.t1.artamonov.tm.exception.field.UserIdEmptyException;
import ru.t1.artamonov.tm.exception.user.AccessDeniedException;
import ru.t1.artamonov.tm.model.Session;
import ru.t1.artamonov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@AllArgsConstructor
public final class SessionService implements ISessionService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    private EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @NotNull
    @Override
    public ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return context.getBean(ISessionRepository.class);
    }

    @NotNull
    @Override
    public Session add(@Nullable Session model) {
        if (model == null) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull Session add(@Nullable String userId, @Nullable Session model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository sessionRepository = getRepository(entityManager);
        try {
            model.setUser(entityManager.find(User.class, userId));
            entityManager.getTransaction().begin();
            sessionRepository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<Session> add(@NotNull List<Session> models) {
        if (models == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull Session model : models) {
                sessionRepository.add(model);
            }
            entityManager.getTransaction().commit();
            return models;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.removeByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return findOneById(id) != null;
    }

    @Override
    public @Nullable List<Session> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository sessionRepository = getRepository(entityManager);
        try {
            return sessionRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository sessionRepository = getRepository(entityManager);
        try {
            return sessionRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable Session findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository sessionRepository = getRepository(entityManager);
        try {
            return sessionRepository.findOneByIdUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Session update(@Nullable Session model) {
        if (model == null) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.update(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Session remove(@Nullable Session model) {
        if (model == null) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.remove(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Session removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session = findOneById(id);
        if (session == null) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.remove(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull Session removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session = findOneById(userId, id);
        if (session == null) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.remove(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
