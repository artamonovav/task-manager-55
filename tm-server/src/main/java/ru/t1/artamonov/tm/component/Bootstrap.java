package ru.t1.artamonov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.api.service.*;
import ru.t1.artamonov.tm.api.service.dto.*;
import ru.t1.artamonov.tm.endpoint.AbstractEndpoint;
import ru.t1.artamonov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @Getter
    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @Getter
    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Getter
    @NotNull
    @Autowired
    private IUserDTOService userService;

    @Getter
    @NotNull
    @Autowired
    private ISessionDTOService sessionService;

    @Getter
    @NotNull
    @Autowired
    private IAuthService authService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @SneakyThrows
    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
    }

    public void run() {
        initPID();
        initEndpoints();
        loggerService.initJmsLogger();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

}
